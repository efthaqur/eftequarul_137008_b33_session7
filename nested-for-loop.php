<?php
/**
 * Nested Loop
 * Date: 10/1/2016
 * Time: 9:29 AM
 */


        for($m = 0; $m < 5; $m++ ){

            echo 'This is outer loop'.$m.'<br>';

            for($n = 0; $n < 3; $n++){

                echo "&nbsp; This is inner loop $n"."<br>";
            }
            echo '<br>';

        }
?>

<br>

<?php
for ($x = 1; $x <= 5; $x++)
{
    for ($y = 1; $y <= $x; $y++)
    {
        echo '*';
    }

    echo "<br>";
}
?>

<br>

<?php

    $ln=5;
    $b =1;
    for ($space = $ln-1; $space>0; $space-- ){

        echo "&nbsp;";

        for($a = 1; $a<=$b; $a++){
            echo "*";
        }
    }

?>

<br>
